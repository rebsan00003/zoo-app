import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private readonly router: Router) { }
  //redirect user to zoo-page
  handleLogin(): void {
    this.router.navigateByUrl("/zoo-animal");
  }



  ngOnInit(): void {
  }

}
