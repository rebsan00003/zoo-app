import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { ZooAnimalsComponent } from './pages/zoo-animals/zoo-animals.component';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/login"
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "zoo-animal",
    component: ZooAnimalsComponent
  }


]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
], // import a module
exports: [
    RouterModule
] // expose module and its features
})
export class AppRoutingModule { }
