import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  @Output() login: EventEmitter<void> = new EventEmitter();

  

  constructor(private readonly router: Router) { }

  ngOnInit(): void {
  }

  //change name? redirect to keycloak to login
  loginWithKeycloak(): void {
    keycloak.login()
    this.router.navigateByUrl("/zoo-animal")


  }

  

}
